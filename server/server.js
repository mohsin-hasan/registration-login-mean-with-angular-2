var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');
const path = require('path');
var config = require('./config');

const jwt = require('jsonwebtoken');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Point static path to dist
// app.use(express.static(path.join(__dirname, './src')));

app.use(expressJwt({
    secret: config.secret,
    getToken: function (req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
            return req.headers.authorization.split(' ')[1];
        else if (req.query && req.query.token)
            return req.query.token;
        return null;
    }
}).unless({ path: ['/users/authenticate', '/users/register', '/users/forget-password', '/users/reset-password'] }));

/*app.use(function (req, res, next) {
	var token;
	if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
		token = req.headers.authorization.split(' ')[1];

	if (token) {
		jwt.verify(token, config.secret, function (err, res) {
			if (err)
				return res.json({ success: false, message: 'Failed to authenticate token.' });
			else {
				req.token = res;
				next();
				// return res;
			}
		});
	}
	else {
		return res.status(403).send({
			success: false,
			message: 'No token provided.'
		});
	}
});*/

app.use('/users', require('./controllers/users.controller'));

var port = process.env.NODE_ENV === 'production' ? 80 : 4000;
var server = app.listen(port, function () {
	console.log('Server listening on port ' + port);
});