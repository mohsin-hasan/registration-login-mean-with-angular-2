const config = require('../config');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const q = require('q');
const mongo = require('mongoskin');
const crypto = require('crypto');

var db = mongo.db(config.connectionString, { native_parser: true });
db.bind('users');

const service = {};

// service.authenticate = authenticate;
// service.getAll = getAll;
// service.getById = getById;
// service.create = create;
// service.update = update;
// service.delete = _delete;

service.authenticate = (username, password) => {
	let deferred = q.defer();

	db.users.findOne({ username: username }, (err, user) => {
		if (err)
			deferred.reject(err.name + ':' + err.message);
		if (user && bcrypt.compareSync(password, user.password)) {
			deferred.resolve({
				_id: user._id,
				username: user.username,
				firstName: user.firstName,
				lastName: user.lastName,
				token: jwt.sign({ sub: user._id }, config.secret)
			});
		}
		else if (user && !bcrypt.compareSync(password, user.password))
			deferred.reject("Incorrect password");
		else
			deferred.reject(username + ' does not exist');
	});

	return deferred.promise;
}

service.forgetPassword = (username) => {
	let deferred = q.defer();

	db.users.findOne({ username: username }, (err, user) => {
		if (err)
			deferred.reject(err.name + ':' + err.message);
		if (user) {
			const payload = {
				resetPassword: user._id
			};

			// let token = jwt.sign(payload, config.secret, {expiresIn: 86400}); // expires in 24 hrs
			let token = crypto.randomBytes(20).toString('hex');
			setResetPasswordToken(user, token)
				.then(function () {
					// console.log("updated user : ", list);
					deferred.resolve(token);
				})
				.catch(function (err) {
					deferred.reject(err);
				});
		}
		else
			deferred.reject(username + ' does not exist');
	});

	return deferred.promise;
}

function setResetPasswordToken(user, token) {
	let deferred = q.defer();

	var userModel = {
		reset_password_token: token,
		reset_password_expires: Date.now() + 86400000
	}

	db.users.findOneAndUpdate({ _id: user._id }, { $set: userModel }, { returnNewDocument: true }, function (err, list) {
		if (err)
			deferred.reject();
		if (list)
			deferred.resolve();
	});

	return deferred.promise;
}

service.resetPassword = (userDetail, token) => {
	let deferred = q.defer();

	db.users.findOne(
		{
			reset_password_token: token,
			reset_password_expires: {
				$gt: Date.now()
			}
		}, (err, user) => {
			if (err)
				deferred.reject(err.name + ':' + err.message);
			if (user) {
				var userModel = {
					password: bcrypt.hashSync(userDetail.password, 10),
					reset_password_token: undefined,
					reset_password_expires: undefined
				}

				// user.save(function (err) {
				// 	if (err)
				// 		deferred.reject(err.name + ':' + err.message);
				// 	else {
				// 		deferred.resolve("Password has been reset.");
				// 	}
				// });

				db.users.update(
					{ _id: mongo.helper.toObjectID(user._id) },
					{ $set: userModel },
					(err, doc) => {
						if (err)
							deferred.reject(err.name + ':' + err.message);
						deferred.resolve("Password has been reset.");
					});
			}
			else
				deferred.reject('Password reset token is invalid or has expired.');
		});

	return deferred.promise;
}

service.getAll = (req, res) => {
	let deferred = q.defer();

	db.users.find().toArray((err, users) => {
		if (err)
			deferred.reject(err.name + ':' + err.message);

		users = _.map(users, (user) => {
			// console.log("user : ", user);
			return _.omit(user, 'password');
		});

		var token;
		if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
			token = req.headers.authorization.split(' ')[1];

		if (token) {
			jwt.verify(token, config.secret, function (err, res) {
				if (err)
					return res.json({ success: false, message: 'Failed to authenticate token.' });
				else {
					req.token = res;
					// next();
					// return res;
				}
			});
		}
		else {
			return res.status(403).send({
				success: false,
				message: 'No token provided.'
			});
		}

		deferred.resolve(users);
	});

	return deferred.promise;
}

service.getById = (_id) => {
	let deferred = q.defer();

	db.users.findById(_id, (err, user) => {
		if (err)
			deferred.reject(err.name + ':' + err.message);

		if (user)
			deferred.resolve(_.omit(user, 'password'));
		else
			deferred.resolve();
	});

	return deferred.promise;
}

service.create = (userDetail) => {
	let deferred = q.defer();

	db.users.findOne({ username: userDetail.username }, (err, user) => {
		if (err)
			deferred.reject(err.name + ':' + err.message);
		if (user)
			deferred.reject('Username "' + user.username + '" is already taken');
		else
			createUser(userDetail).then(() => {
				deferred.resolve();
			}, (err) => {
				deferred.reject(err);
			});
	});

	return deferred.promise;
}

function createUser(userDetail) {
	let deferred = q.defer();

	let user = _.omit(userDetail, ['confirmPassword', 'password', 'hash']);

	user.password = bcrypt.hashSync(userDetail.password, 10);

	db.users.insert(user, (err, doc) => {
		if (err)
			deferred.reject(err.name + ':' + err.message);
		if (doc)
			deferred.resolve();
	});

	return deferred.promise;
}

service.update = (_id, userDetail) => {
	let deferred = q.defer();

	db.users.findById(_id, (err, user) => {
		if (err)
			deferred.reject(err.name + ':' + err.message);

		if (userDetail.username !== user.username) {
			db.users.findOne({ username: userDetail.username }, (err, user) => {
				if (err)
					deferred.reject(err.name + ':' + err.message);
				if (user)
					deferred.reject('Username "' + userParam.username + '" is already taken');
				else
					updateUser(_id, userDetail).then(() => {
						deferred.resolve();
					}, (err) => {
						deferred.reject(err);
					});
			})
		}
	});

	return deferred.promise;
}

function updateUser(_id, userDetail) {
	let deferred = q.defer();

	var userModel = {
		firstName: userDetail.firstName,
		lastName: userDetail.lastName,
		username: userDetail.username
	}

	if (userDetail.password)
		userModel.password = bcrypt.hashSync(userDetail.password, 10);

	db.users.update(
		{ _id: mongo.helper.toObjectID(_id) },
		{ $set: userModel },
		(err, doc) => {
			if (err)
				deferred.reject(err.name + ':' + err.message);
			deferred.resolve();
		});

	return deferred.promise;
}

service.delete = (_id) => {
	let deferred = q.defer();

	db.users.remove({ _id: mongo.helper.toObjectID(_id) }, err => {
		if (err)
			deferred.reject(err.name + ':' + err.message);

		deferred.resolve();
	});

	return deferred.promise;
}

module.exports = service;

