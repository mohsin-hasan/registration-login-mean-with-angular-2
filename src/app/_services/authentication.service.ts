import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {

	constructor(private http: Http) { }

	login(username: string, password: string) {
		return this.http.post('/users/authenticate', { username: username, password: password })
			.map((response: Response) => {
				let user = response.json();
				if (user && user.token)
					localStorage.setItem('loggedInUser', JSON.stringify(user));
				return user;
			});
	}

	logout() {
		localStorage.removeItem('loggedInUser');
	}

	forgetPassword(user) {
		return this.http.post('/users/forget-password', { username: user.username, email: user.email })
			.map(res => res);
	}

	resetPassword(user, token) {
		return this.http.post('/users/reset-password', user, {params: {token: token}})
			.map(res => {
				console.log(res);
				return res;
			});
	}

}
