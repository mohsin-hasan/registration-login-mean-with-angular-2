import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AlertComponent } from './alert/alert.component';

import { AppRoutingModule } from './app-routing.module';

import { AlertService } from './_services/alert.service';
import { AuthGuard } from './_guards/auth.guard';
import { customHttpProvider } from './_services/custom-http.service';
import { AuthenticationService } from './_services/authentication.service';
import { UserService } from './_services/user.service';
import { PasswordValidatorDirective } from './_directives/password-validator.directive';
import { UpdateComponent } from './update/update.component';
import { UserUpdateResolverService } from './_services/user-update-resolver.service';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		RegisterComponent,
		LoginComponent,
		AlertComponent,
		PasswordValidatorDirective,
		UpdateComponent,
		ForgetPasswordComponent,
		ResetPasswordComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		AppRoutingModule,
		HttpModule
	],
	providers: [
		AlertService,
		AuthGuard,
		customHttpProvider,
		AuthenticationService,
		UserService,
		UserUpdateResolverService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
