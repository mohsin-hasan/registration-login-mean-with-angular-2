import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { User } from '../_models/user';
import { UserService } from '../_services/user.service';

@Component({
	selector: 'app-update',
	templateUrl: './update.component.html',
	styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

	user: User;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private userService:UserService
	) { }

	ngOnInit() {
		this.route.data.subscribe((res) => this.user = res.user);
		console.log("User - ", this.user);
	}

	update() {
		console.log("User : ", this.user);
		this.userService.update(this.user).subscribe((res) => {
			console.log("update response: ", res)
		});
	}

}
